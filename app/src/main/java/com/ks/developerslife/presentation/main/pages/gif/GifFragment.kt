package com.ks.developerslife.presentation.main.pages.gif

import android.os.Bundle
import android.transition.ChangeBounds
import android.transition.TransitionManager
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.animation.AccelerateDecelerateInterpolator
import androidx.constraintlayout.widget.ConstraintSet
import androidx.core.content.ContextCompat
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProvider
import androidx.swiperefreshlayout.widget.CircularProgressDrawable
import com.bumptech.glide.Glide
import com.bumptech.glide.load.engine.DiskCacheStrategy
import com.ks.developerslife.R
import com.ks.developerslife.data.network.GifInfo
import com.ks.developerslife.presentation.common.Error
import com.ks.developerslife.presentation.common.Loaded
import com.ks.developerslife.presentation.common.Loading
import com.ks.developerslife.presentation.ext.hide
import com.ks.developerslife.presentation.ext.setColorFilter
import com.ks.developerslife.presentation.ext.show
import kotlinx.android.synthetic.main.gif_fragment.*
import kotlinx.android.synthetic.main.include_error_layout.*
import kotlinx.android.synthetic.main.include_loading_layout.*

class GifFragment : Fragment() {

    private val glide by lazy { Glide.with(this) }
    private val circularProgressDrawable by lazy {
        CircularProgressDrawable(requireContext())
            .apply {
                setColorFilter(ContextCompat.getColor(requireContext(), R.color.colorAccent))
                strokeWidth = 10f
                centerRadius = 30f
                start()
            }
    }

    private var maxCardViewHeight: Int = 0
    private lateinit var viewModel: GifViewModel

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        return inflater.inflate(R.layout.gif_fragment, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        viewModel = ViewModelProvider(this).get(GifViewModel::class.java)

        observeIncomingGifs()
        observePageNumber()
        observeLoadingState()

        nextButton.setOnClickListener { viewModel.onNextClick() }
        backButton.setOnClickListener { viewModel.onBackClick() }
        retryButton.setOnClickListener { viewModel.onRetryClick() }
    }

    private fun observeIncomingGifs() {
        viewModel.gifInfoLiveData.observe(this, { gifInfo ->
            changeCardHeight(gifInfo)
            setGifInfo(gifInfo)
        })
    }

    private fun changeCardHeight(gifInfo: GifInfo) {
        if (gifInfo.height == null
            || gifInfo.width == null
            || gifInfo.height == 0
            || gifInfo.width == 0
        ) return

        // calculate max height one time before first changing
        if (maxCardViewHeight == 0) maxCardViewHeight = gifCardView.measuredHeight

        val ratio = gifInfo.height.toFloat().div(gifInfo.width)
        var newHeight = (gifCardView.width * ratio).toInt()
        if (newHeight > maxCardViewHeight) newHeight = maxCardViewHeight

        val constraintSet = ConstraintSet()
        constraintSet.clone(contentContainer)

        constraintSet.constrainHeight(R.id.gifCardView, newHeight)

        val transition = ChangeBounds()
        transition.duration = ANIMATION_DURATION
        transition.interpolator = AccelerateDecelerateInterpolator()
        TransitionManager.beginDelayedTransition(contentContainer, transition)

        constraintSet.applyTo(contentContainer)
    }

    private fun setGifInfo(gifInfo: GifInfo) {
        gifCommentTextView.text = gifInfo.description
        glide.load(gifInfo.gifURL)
            .placeholder(circularProgressDrawable)
            .error(R.drawable.ic_error_24)
            .diskCacheStrategy(DiskCacheStrategy.ALL)
            .into(gifHolder)
    }

    private fun observePageNumber() {
        viewModel.pageNumberLiveData.observe(this, { pageNumber ->
            backButton.isEnabled = pageNumber > 1
        })
    }

    private fun observeLoadingState() {
        viewModel.loadingStateLiveData.observe(this, { state ->
            when (state) {
                Loading -> {
                    loadingOverlay.show()
                    errorOverlay.hide()
                }
                Error -> {
                    loadingOverlay.hide()
                    errorOverlay.show()
                }
                Loaded -> {
                    loadingOverlay.hide()
                    errorOverlay.hide()
                }
            }
        })
    }

    companion object {
        private const val ANIMATION_DURATION = 100L

        fun newInstance() = GifFragment()
    }

}