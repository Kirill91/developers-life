package com.ks.developerslife.presentation.common

sealed class LoadingState
object Loading : LoadingState()
object Error : LoadingState()
object Loaded : LoadingState()