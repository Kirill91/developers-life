package com.ks.developerslife.presentation.main.pages.gif

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.ks.developerslife.data.network.GifInfo
import com.ks.developerslife.presentation.DI
import com.ks.developerslife.presentation.common.Error
import com.ks.developerslife.presentation.common.Loaded
import com.ks.developerslife.presentation.common.Loading
import com.ks.developerslife.presentation.common.LoadingState

class GifViewModel : ViewModel() {

    private val repository by lazy { DI.gifsRepository }
    private var currentPosition: Int = 1

    val gifInfoLiveData = MutableLiveData<GifInfo>()
    val pageNumberLiveData = MutableLiveData<Int>(1)
    val loadingStateLiveData = MutableLiveData<LoadingState>(Loading)

    init {
        getGifAndShow(currentPosition)
    }

    fun onNextClick() {
        currentPosition++
        getGifAndShow(currentPosition)
    }

    fun onBackClick() {
        if (currentPosition > 0) {
            currentPosition--
            getGifAndShow(currentPosition)
        }
    }

    fun onRetryClick() {
        getGifAndShow(currentPosition)
    }

    private fun getGifAndShow(number: Int) {
        repository.getGif(number)
            .doOnSubscribe { loadingStateLiveData.value = Loading }
            .subscribe(
                {
                    pageNumberLiveData.value = currentPosition
                    gifInfoLiveData.value = it
                    loadingStateLiveData.value = Loaded
                },
                {
                    loadingStateLiveData.value = Error
                }
            )
    }

}