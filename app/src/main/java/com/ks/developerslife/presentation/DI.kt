package com.ks.developerslife.presentation

import com.ks.developerslife.data.cache.GifsCache
import com.ks.developerslife.data.network.GifApi
import com.ks.developerslife.data.network.NetworkProvider
import com.ks.developerslife.data.repository.GifsRepositoryImpl
import com.ks.developerslife.domain.api.GifsRepository

object DI {
    private val networkProvider: NetworkProvider = NetworkProvider()
    private val gifApi: GifApi = networkProvider.gifApi

    private val gifsCache: GifsCache = GifsCache()

    var gifsRepository: GifsRepository = GifsRepositoryImpl(gifApi, gifsCache)
}