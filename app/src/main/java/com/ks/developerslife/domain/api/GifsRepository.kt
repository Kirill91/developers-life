package com.ks.developerslife.domain.api

import com.ks.developerslife.data.network.GifInfo
import io.reactivex.rxjava3.core.Single

interface GifsRepository {

    fun getGif(number: Int): Single<GifInfo>

}