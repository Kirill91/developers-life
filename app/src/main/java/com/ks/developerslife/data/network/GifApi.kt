package com.ks.developerslife.data.network

import io.reactivex.rxjava3.core.Single
import retrofit2.http.GET

interface GifApi {

    @GET("random?json=true")
    fun loadRandomGif(): Single<GifInfo>

}