package com.ks.developerslife.data.network

import com.fasterxml.jackson.annotation.JsonProperty

data class GifInfo(
    @JsonProperty("id")
    val id: Int?,
    @JsonProperty("description")
    val description: String?,
    @JsonProperty("gifURL")
    val gifURL: String?,
    @JsonProperty("width")
    val width: Int?,
    @JsonProperty("height")
    val height: Int?
)