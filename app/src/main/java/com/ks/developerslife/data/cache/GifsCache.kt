package com.ks.developerslife.data.cache

import com.ks.developerslife.data.network.GifInfo

/**
 * Can be replaced with database
 */
class GifsCache {
    private val gifList = mutableListOf<GifInfo>()

    val size: Int
        get() = gifList.size

    fun get(number: Int) = gifList[number]
    fun save(gifInfo: GifInfo) = gifList.add(gifInfo)
}