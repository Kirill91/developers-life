package com.ks.developerslife.data.repository

import com.ks.developerslife.core.ext.schedule
import com.ks.developerslife.data.cache.GifsCache
import com.ks.developerslife.data.network.GifApi
import com.ks.developerslife.data.network.GifInfo
import com.ks.developerslife.domain.api.GifsRepository
import io.reactivex.rxjava3.core.Single

class GifsRepositoryImpl(
    private val gifApi: GifApi,
    private val gifsCache: GifsCache
) : GifsRepository {

    override fun getGif(number: Int): Single<GifInfo> {
        return if (gifsCache.size >= number) {
            Single.just(gifsCache.get(number - 1))
        } else {
            loadRandomGif()
                .doOnSuccess { gifsCache.save(it) }
        }
            .schedule()
    }

    private fun loadRandomGif(): Single<GifInfo> {
        return gifApi.loadRandomGif()
    }
}