package com.ks.developerslife.core.ext

import android.annotation.SuppressLint
import okhttp3.OkHttpClient
import java.security.SecureRandom
import java.security.cert.X509Certificate
import javax.net.ssl.*

/**
Don't use for production
 */
fun OkHttpClient.Builder.debugSSLSocketFactory(): OkHttpClient.Builder {
    val sslSocketFactory: SSLSocketFactory
    val x509TrustManager: X509TrustManager
    try {
        x509TrustManager = object : X509TrustManager {
            @SuppressLint("TrustAllX509TrustManager")
            override fun checkClientTrusted(chain: Array<X509Certificate>, authType: String) {
            }

            @SuppressLint("TrustAllX509TrustManager")
            override fun checkServerTrusted(chain: Array<X509Certificate>, authType: String) {
            }

            override fun getAcceptedIssuers(): Array<X509Certificate> {
                return arrayOf()
            }
        }
        val trustAllCerts = arrayOf<TrustManager>(x509TrustManager)
        val sslContext = SSLContext.getInstance("SSL")
        sslContext.init(null, trustAllCerts, SecureRandom())
        sslSocketFactory = sslContext.socketFactory
    } catch (e: Exception) {
        throw RuntimeException(e)
    }
    return this.sslSocketFactory(sslSocketFactory, x509TrustManager)
        .hostnameVerifier(HostnameVerifier { _: String?, _: SSLSession? -> true })
}